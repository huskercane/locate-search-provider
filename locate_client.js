/*
 * You should have received a copy of the GNU General Public License
 * along with gnome-shell-extension-openweather.
 * If not, see <http://www.gnu.org/licenses/>.LocateClient
  */

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Convenience = Extension.imports.convenience;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const ByteArrays = imports.byteArray;

const LOCATE = "/usr/bin/locate";

class LocateClient {
    constructor(params) {
        this._settings = Convenience.getSettings();
        this._settings.connect("changed", () => {
            this._max_results = this._settings.get_int('max-results');
        });
    }



    get(word, callback, p1, p2) {
        let home_dir = GLib.get_home_dir();

        execCommunicate([LOCATE, word]).then(stdout => {
            let results = [];
            stdout.split('\n').map((element, index) => {
                let el1 = element;
                if (element.startsWith(home_dir)) {
                    element = '~' + element.substring(home_dir.length);
                }
                results.push({
                    id: 'index_' + index,
                    label: element,
                    description: element,
                    url: 'file://' + el1
                });
            });
            if (results.length > 0) {
                callback(null, results, p1, p2);
                return;
            }
            let message = "Nothing found";
            callback(message, null, p1, p2);
        }).catch(function(e) {
            log(e, "SOmethign went wrong");
            log(e.stack, "SOmethign went wrong");
        });
    }
    destroy() {
    }

}
/**
 * Execute a command asynchronously and return the output from `stdout` on
 * success or throw an error with output from `stderr` on failure.
 *
 * If given, @input will be passed to `stdin` and @cancellable can be used to
 * stop the process before it finishes.
 *
 * @param {string[]} argv - a list of string arguments
 * @param {string} [input] - Input to write to `stdin` or %null to ignore
 * @param {Gio.Cancellable} [cancellable] - optional cancellable object
 * @returns {Promise<string>} - The process output
 */
async function execCommunicate(argv, input = null, cancellable = null) {
    let cancelId = 0;
    let flags = (Gio.SubprocessFlags.STDOUT_PIPE |
        Gio.SubprocessFlags.STDERR_PIPE);

    if (input !== null)
        flags |= Gio.SubprocessFlags.STDIN_PIPE;

    let proc = new Gio.Subprocess({
        argv: argv,
        flags: flags
    });
    proc.init(cancellable);

    if (cancellable instanceof Gio.Cancellable) {
        cancelId = cancellable.connect(() => proc.force_exit());
    }

    return new Promise((resolve, reject) => {
        proc.communicate_utf8_async(input, null, (proc, res) => {
            try {
                let [, stdout, stderr] = proc.communicate_utf8_finish(res);
                let status = proc.get_exit_status();

                if (status !== 0) {
                    throw new Gio.IOErrorEnum({
                        code: Gio.io_error_from_errno(status),
                        message: stderr ? stderr.trim() : GLib.strerror(status)
                    });
                }

                resolve(stdout.trim());
            } catch (e) {
                reject(e);
            } finally {
                if (cancelId > 0) {
                    cancellable.disconnect(cancelId);
                }
            }
        });
    });
}
