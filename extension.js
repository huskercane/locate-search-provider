/*
 * You should have received a copy of the GNU General Public License
 * along with gnome-shell-extension-openweather.
 * If not, see <http://www.gnu.org/licenses/>.
  */

const St = imports.gi.St;
const Main = imports.ui.main;
const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const GLib = imports.gi.GLib;
const Clutter = imports.gi.Clutter;
const Util = imports.misc.util;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
var LocateClient = Extension.imports.locate_client;
const Convenience = Extension.imports.convenience;

const Gettext = imports.gettext.domain(Extension.metadata.uuid);
const _ = Gettext.gettext;

class LocateSearchProvider {
    constructor() {

        Gtk.IconTheme.get_default().append_search_path(
            Extension.dir.get_child('icons').get_path());
        this.appInfo = Gio.AppInfo.get_default_for_uri_scheme('https');
        // Fake the name and icon of the app
        this.appInfo.get_name = () => {
            return 'Locate';
        };
        this.appInfo.get_icon = () => {
            let gicon = Gio.icon_new_for_string(Extension.dir.get_child('icons').get_path() + "/locate-icon.svg");
            return gicon;
        };

        // Custom messages that will be shown as search results
        this._messages = {
            '__loading__': {
                id: '__loading__',
                name: _('Locate'),
                description: _('Loading items from Locate, please wait...'),
                // TODO: do these kinds of icon creations better
                createIcon: this.createIcon
            },
            '__error__': {
                id: '__error__',
                name: _('Locate'),
                description: _('Oops, an error occurred while searching.'),
                createIcon: this.createIcon
            }
        };
        // API results will be stored here
        this.resultsMap = new Map();
        this._api = new LocateClient.LocateClient();
        // Wait before making an API request
        this._timeoutId = 0;


    }

    /**
     * Open the url in default app
     * @param {String} identifier
     * @param {Array} terms
     * @param timestamp
     */
    activateResult(identifier, terms, timestamp) {
        let result;
        let command = '';
        // only do something if the result is not a custom message
        if (!(identifier in this._messages)) {
            result = this.resultsMap.get(identifier);
            if (result) {
                if (command === '') {
                    command = 'xdg-open %s';
                }
                Util.trySpawnCommandLine(command.format(result.url));
            }
        }
    }

    /**
     * Run callback with results
     * @param {Array} identifiers
     * @param {Function} callback
     */
    getResultMetas(identifiers, callback) {
        let metas = [];

        for (let i = 0; i < identifiers.length; i++) {
            let result;
            // return predefined message if it exists
            if (identifiers[i] in this._messages) {
                metas.push(this._messages[identifiers[i]]);
            } else {
                // TODO: check for messages that don't exist, show generic error message
                let meta = this.resultsMap.get(identifiers[i]);
                if (meta) {
                    metas.push({
                        "id": meta.id,
                        "name": meta.label,
                        createIcon: (size) => {
                            // let box = new Clutter.Box();
                            let directory_file = Gio.file_new_for_path(meta.label.replace('~/', ''));
                            let icon_names = directory_file.query_info('standard::symbolic-icon', 0, null).get_symbolic_icon().get_names();
                            let icon = icon_names[0];
                            let icon2 = new St.Icon({
                                gicon: new Gio.ThemedIcon({name: icon}),
                                icon_size: size
                            });

                            // box.add_child(icon2);
                            return icon2;
                        }
                    });
                }
            }
        }
        callback(metas);
    }

    /**
     * Search API if the query is a Wikidata query.
     * Wikidata query must start with a 'wd' as the first term.
     * @param {Array} terms
     * @param {Function} callback
     * @param {Gio.Cancellable} cancellable
     */
    getInitialResultSet(terms, callback, cancellable) {
        if (terms != null && terms.length >= 1 && terms[0].length > 3) {
            // show the loading message
            this.showMessage('__loading__', callback);
            // remove previous timeout
            if (this._timeoutId > 0) {
                GLib.source_remove(this._timeoutId);
                this._timeoutId = 0;
            }
            this._timeoutId = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 1500, () => {
                // escape all space in name
                terms.forEach((value, index) => {
                    terms[index] = value.replace(' ', '\ ');
                })
                // now search
                this._api.get(
                    this._getQuery(terms.join(' ')),
                    this._getResultSet.bind(this),
                    callback,
                    this._timeoutId
                );
                return false;
            });
        } else {
            // return an empty result set
            this._getResultSet(null, {}, callback, 0);
        }
    }

    /**
     * Show any message as a search item
     * @param {String} identifier Message identifier
     * @param {Function} callback Callback that pushes the result to search
     * overview
     */
    showMessage(identifier, callback) {
        callback([identifier]);
    }

    /**
     * @param {Array} previousResults
     * @param {Array} terms
     * @param {Function} callback
     */
    getSubsearchResultSet(previousResults, terms, callback) {
        let x = previousResults.filter((e) => {
            let result = this.resultsMap.get(e);
            if (!result || !result.label) {
                return false;
            }
            let index = result.label.indexOf(terms[0]);
            // log("Index of '" + terms[0] + "' in '" + result.label + "' is: " + index + "returning: " + index != -1);
            return index !== -1;
        });
        callback(x);
    }

    /**
     * Return subset of results
     * @param {Array} results
     * @param {number} max
     * @returns {Array}
     */
    filterResults(results, max) {
        return results.slice(0, max);
    }

    /**
     * Return query string from terms array
     * @param {string} terms
     * @returns {String}
     */
    _getQuery(terms) {
        return terms;
    }

    /**
     * Parse results that we get from the API and save them in this.resultsMap.
     * Inform the user if no results are found.
     * @param {null|String} error
     * @param {Object|null} result
     * @param {Function} callback
     * @param {Function} timeoutId
     * @private
     */
    _getResultSet(error, result, callback, timeoutId) {
        let results = [];
        if (timeoutId === this._timeoutId && result && result.length > 0) {
            result.forEach((aresult) => {
                this.resultsMap.set(aresult.id, aresult);
                results.push(aresult.id);
            });
            callback(results);
        } else if (error) {
            // Let the user know that an error has occurred.
            this.showMessage('__error__', callback);
        }
    }

    /**
     * Create meta icon
     * @param size
     */
    createIcon(size) {
        // let box = new Clutter.Box();
        let icon = new St.Icon({
            // gicon: new Gio.ThemedIcon({ name: 'locate-icon' }),
            icon_size: size
        });
        icon.gicon = Gio.icon_new_for_string(`${Extension.path}/icons/locate-icon.svg`);
        // box.add_child(icon);
        // return box;
        return icon;
    }
}

let locateSearchProvider = null;

function init() {
    Convenience.initTranslations();
}

function enable() {
    if (!locateSearchProvider) {
        locateSearchProvider = new LocateSearchProvider();
        Main.overview.viewSelector._searchResults._registerProvider(
            locateSearchProvider
        );
    }
}

function disable() {
    if (locateSearchProvider) {
        Main.overview.viewSelector._searchResults._unregisterProvider(
            locateSearchProvider
        );
        locateSearchProvider = null;
    }
}
